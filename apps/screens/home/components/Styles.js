import { StyleSheet, Dimensions } from 'react-native'
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white'
  },
  spinnerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  searchScreenImageContainer: {
    flex: 1,
    marginTop: deviceHeight / 80
  },
  noResultFoundImageContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  noResultFoundImage: {
    height: deviceHeight / 2.5,
    width: deviceWidth / 2.5
  }

});

export default styles
