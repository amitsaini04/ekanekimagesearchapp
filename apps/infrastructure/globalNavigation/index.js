import {
  createStackNavigator,
} from 'react-navigation';
import stackRoutes from '../../routes/stackRouter'


const GlobalNavigation = createStackNavigator(
  stackRoutes,{
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
}
);

export default GlobalNavigation;