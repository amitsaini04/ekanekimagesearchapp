import Home from '../screens/home'
import * as routes from './constants'
const stackRoutes = {
    [routes.Home]:{screen:Home}
};

export default stackRoutes;