import React from 'react'
import { View, Image, FlatList,  Dimensions, TouchableOpacity } from 'react-native'
import styles from './Styles';
import { StyleProvider, Container, Header, Item, Input, Icon, Left, Spinner, Text, Content, Body, ActionSheet, Root, List, ListItem } from 'native-base'
import commonColor from '../../../../theme/variables/commonColor';
import getTheme from '../../../../theme/components';
const deviceWidth = Dimensions.get('window').width;
import { CachedImage } from 'react-native-cached-image';
const noresultsfoundImage = require("../../../../assets/images/noresultsfound.png")
const HomeRender = ({ imageSearchResponse, gridColumnSize, updateSearchTerm, fetchImages, isLoading, searchTerm, changeGridColumnSize, searchKeywords }) => {
    return (
        <StyleProvider style={getTheme(commonColor)}>
            <Container style={styles.container}>
                <Root>
                    <Header searchBar rounded>
                        <Item>
                            <Icon name="ios-search" />
                            <Input placeholder="Search for Images" value={searchTerm} onChangeText={updateSearchTerm} onSubmitEditing={() => fetchImages(searchTerm)} />
                            <TouchableOpacity onPress={() => ActionSheet.show(
                                {
                                    options: [
                                        'Grid Column Size: 2',
                                        'Grid Column Size: 3',
                                        'Grid Column Size: 4'
                                    ],
                                    title: "Select Grid Column Size"
                                },
                                buttonIndex => {
                                    changeGridColumnSize(buttonIndex)
                                }
                            )}>
                                <Icon name="menu" />
                            </TouchableOpacity>

                        </Item>
                    </Header>
                    {isLoading ? <View style={styles.spinnerContainer}>
                        <Spinner color="#727272" />
                    </View> :
                        searchTerm != "" && imageSearchResponse.totalHits != null ? imageSearchResponse.totalHits != 0 ?
                            <View style={styles.searchScreenImageContainer}>
                                <FlatList
                                    numColumns={gridColumnSize}
                                    keyExtractor={item => item.id}
                                    key={(gridColumnSize)}
                                    data={imageSearchResponse.hits}
                                    onEndReached={() => fetchImages(searchTerm)}
                                    renderItem={({ item }) =>
                                        <View style={{ flex: 1, justifyContent: 'space-evenly', alignItems: 'center', width: deviceWidth / (gridColumnSize + 0.2), height: deviceWidth / (gridColumnSize) }}>
                                            <CachedImage resizeMode="stretch" source={{ uri: item.previewURL, cache: 'force-cache' }} style={{ flexDirection: "row", justifyContent: "space-around", width: deviceWidth / (gridColumnSize + 0.5), height: deviceWidth / (gridColumnSize + 0.5) }} />
                                        </View>
                                    }
                                />
                            </View> :
                            <View style={styles.noResultFoundImageContainer}>
                                <Image resizeMode='stretch' source={noresultsfoundImage} style={styles.noResultFoundImage} />
                            </View> :
                            searchKeywords.length != 0 ? <Content><List>
                                <ListItem itemHeader first>
                                    <Text>Search History</Text>
                                </ListItem>
                                {searchKeywords.map((data, index) => {
                                    return (
                                        <ListItem key={index} onPress={() => updateSearchTerm(data, fetchImages(data))} icon>
                                            <Left>
                                                <Icon name="ios-search" />
                                            </Left>
                                            <Body>
                                                <Text>{data}</Text>
                                            </Body>
                                        </ListItem>
                                    )
                                })}
                            </List>
                            </Content> : <View></View>}
                </Root>
            </Container>
        </StyleProvider>

    )
}

export default HomeRender