import React, { Component } from 'react'
import { NetInfo } from 'react-native'
import HomeRender from './HomeRender'
import * as AsyncStorage from '../../../../common/AsyncStorage'
import TimerMixin from 'react-timer-mixin';
export default class HomeContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imageSearchResponse: { hits: [] },
            isLoading: false,
            isSearchTermChanged: false,
            searchTerm: '',
            gridColumnSize: 2,
            searchKeywords: []
        }
        this.fetchImages = this.fetchImages.bind(this);
        this.updateSearchTerm = this.updateSearchTerm.bind(this)
        this.changeGridColumnSize = this.changeGridColumnSize.bind(this)
    }

    componentDidMount() {
        AsyncStorage.fetchSearchKeywords((searchKeywords) => { this.setState({ searchKeywords: searchKeywords }) });
    }

    fetchImages(searchTerm) {
        if (searchTerm == "") {
            return;
        }
        TimerMixin.requestAnimationFrame(
            () => {
                if (this.state.isSearchTermChanged) {
                    this.setState({ isLoading: true, imageSearchResponse: { hits: [] } });
                }
                var searchKeywords = this.state.searchKeywords
                if (!searchKeywords.includes(searchTerm)) {
                    searchKeywords.push(searchTerm)
                    this.setState({ searchKeywords: searchKeywords })
                    AsyncStorage.saveSearchTerm(this.state.searchKeywords)
                }
                NetInfo.isConnected.fetch().then(isConnected => {
                    if (!isConnected) {
                        AsyncStorage.fetchResultFromAsyncStorage(searchTerm, (resultFromAsyncStorage) => {
                            this.setState({ imageSearchResponse: resultFromAsyncStorage, isLoading: false, isSearchTermChanged: false });
                        })
                    }
                    else {
                        var imageSearchResultLength = this.state.imageSearchResponse.hits.length
                        var currentPageNumber = Math.ceil(imageSearchResultLength / 20)
                        var nextpageNumber = !this.state.isSearchTermChanged ? Math.ceil((imageSearchResultLength / 20 + (imageSearchResultLength == this.state.imageSearchResponse.totalHits ? 0 : 1))) : 1
                        if (currentPageNumber == nextpageNumber) {
                            return;
                        }
                        var url = 'https://pixabay.com/api/?key=9796625-1f5e5a3a99273739ebe356fd8&q=' + searchTerm + '&image_type=photo&page=' + nextpageNumber
                        try {
                            fetch(url)
                                .then((response) => {
                                    if (response.ok) {
                                        return response.json()
                                    }
                                })
                                .then((responseJson) => {
                                    if (responseJson != null && responseJson.hits != null) {
                                        responseJson.hits = !this.state.isSearchTermChanged && imageSearchResultLength > 0 ? this.state.imageSearchResponse.hits.concat(responseJson.hits) : responseJson.hits
                                        this.setState({ imageSearchResponse: responseJson, isLoading: false, isSearchTermChanged: false });
                                        AsyncStorage.saveSearchResults(searchTerm, responseJson)
                                    }
                                })
                                .catch((error) => {
                                    this.setState({ isLoading: false, isSearchTermChanged: false });
                                    console.error(error);
                                });
                        }
                        catch (error) { }
                    }
                });
            })
    }

    updateSearchTerm(searchTerm, cb) {
        this.setState({ searchTerm: searchTerm, isSearchTermChanged: true, imageSearchResponse: { hits: [] } })
        if (cb) {
            cb();
        }
    }

    changeGridColumnSize(gridColumnButtonIndex) {
        this.setState({ gridColumnSize: gridColumnButtonIndex + 2 })
    }

    render() {
        return (
            <HomeRender imageSearchResponse={this.state.imageSearchResponse} gridColumnSize={this.state.gridColumnSize} updateSearchTerm={this.updateSearchTerm}
                fetchImages={this.fetchImages} isLoading={this.state.isLoading} searchTerm={this.state.searchTerm} changeGridColumnSize={this.changeGridColumnSize}
                searchKeywords={this.state.searchKeywords} />
        );
    }
}