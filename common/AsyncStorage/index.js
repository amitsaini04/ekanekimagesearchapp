import { AsyncStorage } from 'react-native'
import * as constants from './constants'
export async function fetchResultFromAsyncStorage(searchTerm, cb) {
    try {
        var searchResult = JSON.parse(await AsyncStorage.getItem(searchTerm))
        if (searchResult === null) {
            searchResult = []
        }
        cb(searchResult)
    }
    catch (error) {
    }
}

export async function saveSearchResults(searchTerm, results) {
    try {
        await AsyncStorage.setItem(searchTerm, JSON.stringify(results))
    }
    catch (error) {
    }
}

export async function saveSearchTerm(searchKeywords) {
    try{
        await AsyncStorage.setItem(constants.SEARCH_KEYWORD_DATA, JSON.stringify(searchKeywords))
    }
    catch (error) {
    }
}

export async function fetchSearchKeywords(cb) {
    try {
        var searchKeywords = JSON.parse(await AsyncStorage.getItem(constants.SEARCH_KEYWORD_DATA))
        if (searchKeywords === null) {
            searchKeywords = []
        }
        cb(searchKeywords)
    }
    catch (error) {
    }
}